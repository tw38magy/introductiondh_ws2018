# Learn Markdown
## It's really not that difficult

But always remember you need two spaces  
for a linebreak. And a double return

for a new paragraph.

> Markdown you can use now. (Yoda)

You can use
* *emphasis*
* **bold**

and/or 

1. [links](https://docs.google.com/presentation/d/1dqF9OzMRJPgXJVdf9WWKKMe5wC0ATekJOdoj3Ihjtp0/edit?usp=sharing)
2. sub-lists
    * like so
    * and so

All in one `file.md`

And even ![images](https://upload.wikimedia.org/wikipedia/commons/0/05/Cat_trotting%2C_changing_to_a_gallop.gif)

If that all fails you can use html within markdown, but no markdown in that html:

<div>**doh!**</div>

# Ich mag Hashtags
## Aber Doopelhashtags sind auch gut
> Ein größer als ist auch nicht schlecht.
wenn man nicht zweimal enter drückt gibt es kein linebreak

Das tue ich hier nun. Hier schreibe ich ohne irgendeine Modifikation
* Hahaha ich nutze ein süßes kleines Sternchen
* Und gleich noch eins HAha Hihi Huhu UUUUUNNNNDDD jetzt *Steht hier ein text eingebettet links und rechts von einem Stern*

**DIESER TEXT LIEBT STERNCHEN SO SEHR, DASS IHN JEWEILS 2 STERNCHEN UMARMEN AUF JEDER SEITE**

1. steht hinter einer Nummerierung
2. Steht hinter der zweiten Zahl
3. Jetzt kommt gleich ein Link
4. [link zu einem Tollen Bild in ekckigen Klammern](https://thumbs.dreamstime.com/z/gro%C3%9Fes-auge-erzeugt-auf-wei%C3%9F-78307126.jpg)

`what tha fuck is this shit.md`

![hahahihi bild](https://pbs.twimg.com/profile_images/558305026641502208/EsxS3T59_400x400.jpeg)